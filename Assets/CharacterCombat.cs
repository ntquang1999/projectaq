using EasyCharacterMovement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterCombat : MonoBehaviour
{
    private Character character;

    private void Start()
    {
        character = GetComponent<Character>();
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0)) 
        {
            character.GetAnimator().SetTrigger("Attack");
        }
    }
}
